Copyright 2016-2019, by Stefan Boresch
This work is licensed under the Creative Commons Attribution 4.0
International (CC BY 4.0). To view a copy of the license, visit
https://creativecommons.org/licenses/by/4.0/

This directory contains input files to compute the relative solvation free
energy between ethane and methanol. They are intended to demonstrate the
basics of setting up free energy simulations in CHARMM, but should not be
used as templates for real work. Various settings were chosen not for
correctness but to reduce the computational effort. Some of them adversely
affect the result; i.e., the final result (-8.7kcal/mol) is quite far off
from the expected experimental value (-6.9kcal/mol). If you want to
reproduce correct results (results agreeing with experiment), please run
the scripts in directory ../correct

0. Preface for 2019
===================

(i) Ideally, PERT would/should have been "retired" by now. Its main problem is
lack of speed, so it's not useful for large scale applications anymore. On
the other hand, it mostly does what the user tells it to do. For this
course / as a tutorial it should still suffice. Unfortunately, you'll also
have to learn its "quirks". On the other hand, once you get the hang of it,
e.g., trying out things with dummy atoms is quite straightforward, and (for
better or worse) you are in control!

(ii) Given the emphasis I put in 2019 on the correct treatment of dummy
atoms, the single topology (st) example in this directory is about as wrong
as one can get -- nevertheless, this seems to be what most programs
actually do at the moment. As I point out in the lecture, one cannot get
dual topology (dt) fully right at all. I add a directory

 ./dummy_considerations

which outline more correct treatments of dummy atoms for this case. 

I. Some generic background on PERT. You activate it by
======================================================

PERT SELE <selection> END

where <selection> should contain the "region" affected by the mutation;
in our case this is the solute. If you mutate an amino acid in a protein,
<selection> would be that residue, if you mutate a ligand, <selection> would
be the ligand. The purpose of this selection is to avoid computing
identical interactions twice, such as water-water interactions in our case.

Before issuing the PERT statement, your system should represent the
initial state. The PERT command initiates "backup" copies of various
internal data structures, i.e., the system as you have set it up. This
"backup" copy becomes the initial state lambda = 0. Next, you modify
the PSF by whatever means to transform your initial into the desired
final state. Some commands would be

   SCALar CHARge ..
   SCALar TYPE ..
   PATC ..

In fact, you can even delete everything and read in a new PSF. Note, however,
that the sequence and number of atoms has to be identical in the two PSFs.
This strategy is utilized, e.g., by the FESetup site/tool, but I would
recommend it only as a last resort.

Once PERT is activated, energy, minimization and dynamics
commands understand additional options, in particular
LAMB <value> LSTA <value> LSTO <value>, e.g.,

  ENER lamb 0.3 lsta 0 lsto 1

will compute the energy of the system at lambda = 0.3 (see pert.doc for many
more details).

The LSTA/LSTO options used to be relevant only for molecular dynamics
(DYNAmics), but for some time now you should set them to 0 and 1,
respectively, when computing an energy or minimizing.  In dynamics,
they have the following role: E.g.,

  DYNA ... lamb 0.3 lsta 0.2 lsto 0.4 ..

carries out dynamics at lambda = 0.3. At each step, thermodynamic
perturbation / free energy perturbation (FEP) is carried out from
0.3->0.2 and 0.3->0.4.  In addition, dU/dl at lambda=0.3 is accumulated. At the end
of the dynamics run, you get the free energy difference between lambda
= LSTA = 0.2 and lambda = LSTO = 0.4 from thermodynamic perturbation
based on configurations sampled at lambda=0.3, as well as <dU/dl>,
scaled by (LSTO-LSTA=0.4-0.2), i.e. in the example <dU/dl>_{l=0.3} *
0.2. Thus, the simulation at lambda=0.3 is used to obtain two
estimates for the free energy difference between 0.2 and 0.4, using
free energy perturbation and thermodynamic integration.

II. Back to the specific examples:
==================================

0) All PSFs and coordinate files are provided. The scripts setup.inp
and setup_solv.inp can/could be used to regenerate them.  It may be
insightful to look at setup.inp and the extra steps required when
building the dual topology residue 'dual'.

1) The basic scripts are:

    gasp_dt_ti.inp   (dual topology)
    solv_dt_ti.inp.

    gasp_st_ti.inp   (single topology)
    solv_st_ti.inp

The meaning of gasp/solv is hopefully obvious, st = single topology
and dt = dual topology. 

Dual topology uses the special residue 'dual', see toppar/rtf.dat.
(Here you may want to take a close look at what setup.inp does with
resi=dual.)

Single topology starts from real ethane and
mutates it into methanol with two dummy atoms "still attached".
The additional parameters needed for the dummy atoms are included in
toppar/params.dat.

By default, the scripts operate _without_ soft cores (without the PSSP
option).  This is OK in the gas phase, less so in solution. To
activate PSSP, pass pssp:1 from the command line. Ethane to methanol
is so trivial that you can obtain somewhat acceptable results without soft
cores -- hence, running the simulation with and without soft cores may
prove insightful as to the effect/utility of soft cores, in particular
on the shape of <dU/dl>!!

Aside from demonstrating the principal differences between single and
dual topology, the scripts also illustrate different ways to
accomplish alchemical mutations and how dummy atoms can be
handled/"generated". Some comments/warnings: While SCALar RSCAle (dual
topology) is neat, it requires the use of the slow energy routines
(FAST OFF) and is really too slow for any serious work. Having an
explicit dummy atom type (as used in single topology) is more efficient, but
requires you to provide bonded parameters for the dummy atoms, which
quickly can become convoluted. Tools/Sites like FEsetup try to help
with this ..

The underscore '_ti' indicates that the scripts really carry out
thermodynamic integration (TI). Specifically, this is done by running
PERT in "AUTO" mode, where all lambda states are run through by a
single script and integrated by the trapezoidal rule (despite all
recommendations to use something better, but see below).

Since you are likely _not_ to use AUTO mode in real work, take
the respective options either as a black box, or peruse the documentation --
having the actual example should help. In practice, you are likely to
run individual simulations at each lambda value -- which you can run
simultaneously on a cluster. This is outlined next, and there I give
some additional background on PERT options.

If you run the scripts with the provided default lengths and in
solution with PSSP turned on (pssp:1), you get for dual topology dAgas
of appr. 5.7kcal/mol, dAsolution = -3.1+-0.3kcal/mol, and for single
topology dAgas = 5.8kcal/mol and dAsolution = -2.9+-0.1kcal/mol.
Thus, the resulting ddAsolv is -8.8 and -8.7 kcal/mol, respectively,
deviating significantly from the experimental value of -6.9 kcal/mol.

2) For the calculations in solution, I also provide somewhat more
flexible scripts which require some manual postprocessing. The
advantage is that different lambda states can be run in parallel on
different cores/computers; individual lambda values can be freely
chosen and added at will. One could also adapt those easily to use
Bennett instead of TI: write trajectories and set up the needed
post-processing.

The scripts are

   solv_dt_step.inp
   solv_st_step.inp

for dual and single topology. There are also shell scripts
solv_[sd]t_step.sh which can be used to start them in a loop. Note:
command names in there are specific to my machines; also, you may want
to start all steps simultaneously on a cluster. The core of the
scripts is identical to the earlier ones, but each single run carries
out only dynamics at a particular value of lamba. The scripts need to
know how many lambda steps you intend to use (@nlamba, default is 21,
but you can overide from the outside). This number is used to compute
the stepwidth, i.e. @lspac = 1 / (@nlambda - 1). You have to pass the
parameter IDX from the outside, which is used to compute

  lamb = @lspac * @idx

Obviously, 0 <= @idx < @nlambda !!

With these parameters in place, the script carries out two MD
simulations (at the same lambda value). The first is equilibration (no
free energy related quantities collected), followed by the production
run, in which dU/dl is accumulated for every MD step. Since we set
LSTA 0.0 LSTO 1.0, the thermodynamic perturbation result is
meaningless; by contrast, we get the pure <dU/dl> without scaling.  At
the end of the output, there is a printout starting with PAVE>, we
want the fourth field (awk '/PAVE>/ {print $4}' output); this is
<dU/dl> for this lambda value. Thus, you end up with, e.g., 21 <dU/dl>
values, which you now have to integrate. Suppose you have a text file
dudl.dat containing them in order (first line is <dU/dl> lambda=0,
second line is <dU/dl> lambda = 0.05 etc), then the script integ1.awk
carries out Simpson's rule (cf. http://dx.doi.org/10.1002/jcc.21712
concerning the choice of integration methods in TI). Suppose you have
run solv_dt_step.sh with unmodified options, then the following
sequence of commands gives you the overall free energy difference

for i in {0..20}
do
awk '/PAVE>/ {print $4}'  output/dt/1/out.$i.out
done > dudl.dat

awk -f integ1.dat dudl.dat

Running the stepwise protocol with the default lengths, I obtained
dAsolution of -2.85 and -2.82 kcal/mole for dual and single topology,
respectively, improving the overall ddAsolv slightly. Again, please
take a look at the setup in ../correct and, ideally, run the calculations;
in this case you should end up with ddAsolv = -6.8, in excellent agreement
with the experimental value!

In the particular case, single and dual topology obtained for gas
phase and solution, respectively, are quite similar. This is somewhat
of a coincidence -- cancellation of the effects of dummy atoms is
achieved only in double free energy differences. If you plot the shape
of <dU/dl> as a function of lambda for single and dual topology,
you'll see that the two approaches differ significantly!

------------------------------------------------------------------------------

Appendix: Commented snippets from CHARMM output:
================================================

A) PERT simulation at a single lambda value
-------------------------------------------

The following is taken from the output of

   charmm pssp:1 idx:0 -i solv_dt_ti.inp ..

where pssp:1 activates the use of soft-cores, and idx:0 is translated
into lambda=0

...
 PERTURBATION> Free energy perturbation results:
 PERTURBATION> results, LSTART=    0.000000  LSTOP=    1.000000  LLAST=    0.000000 Number of steps used=    200000

[LSTART and LSTOP were set to 0 and 1 in the input file. LLAST is the lambda
value at which this (part of the) simulation was run]

PERTURBATION> result: EXPAVE=0.117613E+33 0.100000E+01 EXPFLC=0.500825E+35 0.000000E+00 DIFAVE=    3.717859 DIFFLC=    6.752550
 PERTURBATION> TP Windowing result, EPRTOT=  -53.686515 EFORWARD=  -53.686515 EBACKWARD=    0.000000 EPREFF=   -9.934785 EPREFB=    0.000000

[This summarizes the thermodynamic perturbation (TP) results -- pretty meaningless in this case]

PERTURBATION> TI Windowing result, EPRTOT=   -6.216926  EFORWARD=   -6.216926 EPREF=   -9.934785

[Summary of the thermodynamic integration result; again, meaningless as
we don't want dA, but <dU/dl>_l at the particular lambda value]

 PSSP> results, LSTART=  0.0000  LSTOP=  1.0000  LLAST=  0.0000 NSTEPS=   200000
 PSSP> DLJDLA=   -6.678941, DCODLA=   -3.708765
 PSSP> DLJTOT=   -6.678941, DCOTOT=   -3.708765

[Some "debug" output from the soft-core code --- useful to check if PSSP
was activated at all!]

 PERTRES> LSTART  =     0.00000 LSTOP=     1.00000 EPRTOT=    -6.21693
          EFORWARD=    -6.21693 EPREF=    -9.93478 DIFAVE=     3.71786
          DIFFLC  =     6.75255

[Some internal characteristics from the original author of PERT
(Bernard Brooks, NIH), which I, admittedly, do not understand.]

PERTURBATION> Averages for the last   200000  steps:

[In this case, this is all there is -- if this were auto mode, such output
would appear whenever lambda was changed.]

PAVE DYN: Step         Time      TOTEner        TOTKe       ENERgy  TEMPerature
PAVE PROP:             GRMS      HFCTote        HFCKe       EHFCor        VIRKe
PAVE EXTERN:        VDWaals         ELEC       HBONds          ASP         USER
PAVE IMAGES:        IMNBvdw       IMELec       IMHBnd       RXNField    EXTElec
PAVE PRESS:            VIRE         VIRI       PRESSE       PRESSI       VOLUme
 ----------       ---------    ---------    ---------    ---------    ---------
PAVE>   200000      0.00000     -6.21693      0.00002     -6.21693      0.00000
PAVE PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PAVE EXTERN>       -5.40256     -1.51245      0.00000      0.00000      0.00000
PAVE IMAGES>        0.80364     -0.10556      0.00000      0.00000      0.00000
PAVE PRESS>         0.00000     -6.63401      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------

[The PAVE entries are <dU/dl>_llast * (lstop - lstart). Since lstop -
lstart = 1, this is the raw derivative <dU/dl>_llast=0, i.e., the
output we want.]

PERTURBATION> Fluctuations for the last   200000  steps:
PFLC>   200000      0.00000      6.75438      0.14307      6.75255      0.00000
PFLC PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PFLC EXTERN>        6.15260      2.43886      0.00000      0.00000      0.00000
PFLC IMAGES>        0.58174      2.20050      0.00000      0.00000      0.00000
PFLC PRESS>         0.00000      6.90198      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------

[The corresponding fluctuation]

PERTURBATION> TOTALS since last reset:
PTOT>   200000      0.00000     -6.21693      0.00002     -6.21693      0.00000
PTOT PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PTOT EXTERN>       -5.40256     -1.51245      0.00000      0.00000      0.00000
PTOT IMAGES>        0.80364     -0.10556      0.00000      0.00000      0.00000
PTOT PRESS>         0.00000     -6.63401      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------

[If this were a job with multiple lambda values, this would be the cumulative
sum of PAVE entries up to this point. In the present case PTOT=PAVE]


B) Example from auto mode (output from gasp_dt_ti.inp)
------------------------------------------------------

...
 PERTURBATION> results, LSTART=    0.975000  LSTOP=    1.000000  LLAST=    1.000000 Number of steps used=      9000
...

[This is from the final step of the auto mode run, llast = 1]

 PERTURBATION> Averages for the last     9000  steps:
PAVE DYN: Step         Time      TOTEner        TOTKe       ENERgy  TEMPerature
PAVE PROP:             GRMS      HFCTote        HFCKe       EHFCor        VIRKe
PAVE EXTERN:        VDWaals         ELEC       HBONds          ASP         USER
PAVE PRESS:            VIRE         VIRI       PRESSE       PRESSI       VOLUme
 ----------       ---------    ---------    ---------    ---------    ---------
PAVE>     9000      0.00000      0.16537     -0.00000      0.16537      0.00000
PAVE PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PAVE EXTERN>        0.00296      0.16241      0.00000      0.00000      0.00000
PAVE PRESS>         0.00000      0.04602      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------
 PERTURBATION> Fluctuations for the last     9000  steps:
PFLC>     9000      0.00000      0.01081      0.00258      0.01050      0.00000
PFLC PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PFLC EXTERN>        0.00076      0.01042      0.00000      0.00000      0.00000
PFLC PRESS>         0.00000      0.00646      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------
 PERTURBATION> TOTALS since last reset:
PTOT>     9000      0.00000      6.75722     -0.00001      6.75722      0.00000
PTOT PROP>          0.00000      0.00000      0.00000      0.00000      0.00000
PTOT EXTERN>        0.11605      6.64117      0.00000      0.00000      0.00000
PTOT PRESS>         0.00000      1.88470      0.00000      0.00000      0.00000
 ----------       ---------    ---------    ---------    ---------    ---------
...

Here PAVE = <dU/dl>_llast=1.0 * (1.000 - 0.975), i.e. dU/dl scaled by
(LSTOP - LSTA).

The PTOT entry is the cumulative sum of all PAVE outputs up and including
this point. If the user made sure that LSTA/LSTO at each step was
set correctly, then the final PTOT value is the free energy difference
of interest!
