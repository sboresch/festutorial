Find here modified versions of gasp_st_ti.inp and toppar/params.dat that
result in a cleaner treatment of the dummy atoms. You will have to adapt
solv_st_ti.inp accordingly yourself.

No "correct" method exists for dual topology.

That being said, the overall relative solvation free energies you will obtain
by the various methods are quite similar. The individual free energy differences (gas phase vs. solvent) will, however, change noticeably!

1) no UB terms on angles involving dummies. See

          gasp_st_ti_noub.inp  params_noub.dat

2) just keeping one 'superflous' angle term to avoid deformation of the
OHD2 group (cf. presentation). This corresponds to the setup in the 'correct'
folder.

          gasp_st_ti_best.inp  params_best.dat
