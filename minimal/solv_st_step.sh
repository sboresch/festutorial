#!/bin/bash
# execute solv_st_step.inp
lneq=100000
lnstep=200000
nlambda=21
pssp=1
rand=${1:-1}
mkdir -p "./output/st/${rand}"
start=0
stop=$((${nlambda}-1))
for ((i=${start};i<=${stop};i++))
do
    echo "idx:$i pssp:${pssp} lneq:${lneq} lnstep:${lnstep} nlambda=${nlambda} rand:${rand}"
    charmm_mscale_sccdftb idx:$i pssp:${pssp} lneq:${lneq} lnstep:${lnstep} nlambda=${nlambda} rand:${rand} -i solv_st_step.inp > output/st/${rand}/out.$i.out
done

