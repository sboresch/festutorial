* generate starting coordinates for etha/meoh/dual
* the resi to generate has to be passed as @resi
* Stefan Boresch, University of Vienna, June 2012
* adapted Aug/Sep 2016
*

! Getting 'dual' (ethane/methanol hybrid) right, in particular being
! able to build initial coordinates requires some tweaking and 'cooperation'
! from the RTF. If anyone knows a smarter way to do this, let me know

! NB: dual as built is etha with --OD-HD dangling off;
!     need to set LJ of --OD-HD to zero

if @resi eq dual fast off

read rtf card name toppar/rtf.dat

read para card name toppar/params.dat

read sequ card
* setup one @resi
*
1
@resi

! the dual topology ethane/methanol hybrid is a bit strange ..
if @resi eq dual bomlev -2

gene @resi first none last none setup

if @resi eq dual then
   ! generate doesn't create dihedrals between the ethane and methanol
   ! part although I don't know exactly why; however, there is a spurious
   ! angle which we have to remove

   ! remove spurious angles and dihedrals between ethane and methanol
   ! part. Interestingly, generate doesn't create dihedrals between 
   ! the ethane and methanol part although I don't know exactly why
   define methanol sele atom dual 1 og .or. atom dual 1 hg1 show end
   define ethane sele  atom dual 1 c2 .or. atom dual 1 h4 .or. -
   	                                   atom dual 1 h5 .or. -
           				   atom dual 1 h6  -
          show end
   dele angl sele ethane end -
             sele methanol end sort
   dele dihe sele ethane end -
   	     sele methanol end sort ! redundant, but may be needed in 
                                    ! other cases ...
   bomlev 0
endif
	
if @resi eq meoh then
   set atom1 hb1
   set atom2 cb
   set atom3 og
else
   set atom1 h1
   set atom2 c1
   set atom3 c2
endif

! ic sequence in general is now a bit of a primadonna; hence ..
bomlev -2
ic para
print ic
ic seed 1 @atom1 1 @atom2 1 @atom3
ic buil
bomlev 0

print coor

! nb: for dual hb gets quite confused 
hbuild sele hydr end

print coor

if @resi eq dual -
   scalar rscale set 0. sele atom @resi 1 og .or. -
                             atom @resi 1 hg1 end

energy
mini sd   nstep 50
mini nrap nstep 10
if @resi eq dual set ehyb ?ener

write psf card name psfcoor/@resi.psf
* psf of @resi in gas phase
*

write coor card name psfcoor/@resi.crd
* minimized starting coors of @resi in gas phase
*

write coor pdb name psfcoor/@resi.pdb
* minimized starting coors of @resi in gas phase
*

if @resi eq dual then
   ! just to get a feeling let's retransform this into real ethane

   dele atom sele methanol end sort

   update inbfrq -1
   mini nrap nstep 10 inbfrq 0
   set eeth ?ener

   ! this is the energy difference between dual topology ethane 
   ! and real ethane ..
   calc ediff = @ehyb - @eeth
endif

stop
