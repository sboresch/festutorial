(c) 2016-2019 Stefan Boresch
This work is licensed under the Creative Commons Attribution 4.0
International (CC BY 4.0). To view a copy of the license, visit
https://creativecommons.org/licenses/by/4.0/

Input for computing relative solvation free energies between
ethane and methanol using

  1) cgenff force field 3.0.1 (from 2016)
  2) Water box with 1,000 water molecules
  3) PME

The scripts provided here are intended as templates for your own work.

Running these scripts leads to results in good agreement with experiment
(ddAsolv(ethane->methanol)=-6.9kcal/mol).  Feel free to carry out the
computations on your own equipment. 

* The mutation is carried out by single topology. Thus, starting point
is ethane, which you can build with setup.inp

* The gas phase script,

  gas_st_ti.inp

is quite similar to the one in ../minimal. The mutation is
adapted to the force field used; the parameters needed for the dummy
atoms are read in from a separate stream file. Further, calculations
are set up with infinite cutoff. The mutation steps are put in
a stream file etha_meoh.str, which is reused in the solvation calculations.

The script further illustrates the deletion of superfluous bonded terms
involving dummy atoms. From a theoretical point of view, only "non-redundant"
terms, e.g., one bond stretching, one angle bending and one dihedral angle term
per dummy atom are required. At the same time, however, it is advantageous to
keep the geometry/shape of a moiety containing dummy atoms similar to
the geometry of the respective other endpoint, where there are no dummy atoms.
For this reasons, no angles involving dummy atoms at the methanol endpoint
are removed. For a full discussion of these arcane details see, e.g.,
http://dx.doi.org/10.1080/08927020211969; the 2019 presentation also went
into great length on these details!

Two changes compared to 2016: The parameters for the D(ummy)-O-D angle
have been adapted / corrected (so that the true minimum methanol energy
is reproduced). Further, all dihedrals to dummies are deleted.

The result I obtain is dA = 7.65(+-0.10) kcal/mol.

* The script setup_solvent places ethane into a pre-equilibrated water
box, resulting in ethane + 997 waters. In principle, one should reequilibrate
at constant pressure, but the effects are miniscule.

* A single step of the free energy calculation in solvation is carried
out by solv_st_step.inp. You have to pass the parameter IDX, which may range
from 0 to nlambda-1; the script then computes lambda from this parameter.
Use of soft-cores is hardwired.

For, e.g., nlambda = 21, you have to run

for i in {0..20}
do
   charmm idx:$i -i solv_st_step.inp > output/out.$i
done

You could, of course, run all jobs in parallel on a cluster. Also, using
all available CPU cores per machine and job speeds up things considerably.

The <dU/dl> of the run can be obtained by running 

   awk '/PAVE>/ {print $4}' <output_file>

Depending on your settings, you end up with, e.g., 11 or 21 output files,
and, hence, 11 or 21 <dU/dl> values. The provided script integ1.awk carries
out integration using Simpson's rule. Suppose you have output files
out.0, out.1, ..., out.20 in directory output. Then you can obtain the
free energy difference by running:

for i in {0..20}
do
awk '/PAVE>/ {print $4}' out.$i
done | awk -f integ1.awk

I obtain dAsolution 0.81 kcal/mole; thus, combining with the gas phase,
ddAsolv = dAsolution - dAgas = 0.81 - 7.65 = -6.84 kcal/mol, in excellent
agreement with the experimental value.

Using the default settings, a single lambda steps took me ca 90 minutes on
4 CPU cores (mpirun -np 4 ..)
