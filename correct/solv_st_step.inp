* Stefan Boresch, Aug/Sep 2016
* single topology calculation in solution, ethane -> methanol
* Use of soft cores set by default
* Just run at a single lambda value, derived from @idx
* You can also finetune number of equil. / prod. steps
* these are production conditions
*

! ad @idx: this should be an integer, from which the actual lambda
! is computed; see calc lamb = ..  below 

read rtf card name toppar/top_all36_cgenff.rtf
read para card name toppar/par_all36_cgenff.prm flex
stream toppar/toppar_water_ions.str
! use a custom made stream file to provide dummy parms
stream toppar/etha_meoh_dummy.str

read psf card name psfcoor/etha_solv.psf
read coor card name psfcoor/etha_solv.crd

pert sele segi etha end

! mutation
stream etha_meoh.str

set bxl  31.1032 ! consensus box length

set ctofnb 12.
set ctonnb 10.
set cutnb  15.
set cutim  @cutnb
crystal define cubic @bxl @bxl @bxl 90.00 90.00 90.00
crystal build Noper 0 cutoff @cutim
image byres xcen 0.0 ycen 0.0 zcen 0.0 sele all end

nbonds ctonnb @ctonnb ctofnb @ctofnb cutnb @cutnb cutim @cutim -
  atom shif vatom vswitch -
  inbfrq -1 imgfrq -1 -
  ewald pmew kappa 0.34 spline order 6 fftx 32 ffty 32 fftz 32

shake bonh sele resname tip3 end para

! some energy checks 
energy lsta 0.0 lsto 1.0 lamb 0.  inbfrq 1 imgfrq 1 pssp ! Note:
   ! (1) force list update, (2) set PSSP (soft cores)
energy lsta 0.0 lsto 1.0 lamb 1.  inbfrq -1 imgfrq -1 ! back to heuristic update
energy lsta 0.0 lsto 1.0 lamb 0.5

! Number of lamba points (including the 2 end points), e.g. 11 or 21
if @?nlambda .eq. 0 set nlambda 21 ! 0 <= @idx <= (nlambda - 1)
! equil simulation length per lambda point
if @?lneq .eq. 0 set lneq  100000
! production simulation length per lambda point
if @?lnstep .eq. 0 set lnstep  200000
! Calculate spacing between lamba points 
calc lspac = 1 / (@nlambda-1)
calc lamb = @lspac * @idx

if @?rand .eq. 0 set rand 1
scalar fbeta set 5. sele all end ! for langevin dynamics

! run equil + production dynamics
! to extend this to Bennett, write trajectory during prod. dynamics

open unit 11 writ form name output/st/@{rand}/rest.@idx.0
dyna leap lang start -
     tbath 300. rbuf 0. ilbfrq 0 iunrea -1 iunwri 11 firstt 240. - 
     nstep @lneq iuncrd -1 iseed @rand @rand @rand @rand -
     npri 10000 time 0.001 iprfrq 20000 echeck 0. -
     lsta 0. lamb @lamb lsto 1. psta @lneq pwind pssp

open unit 10 read form name output/st/@{rand}/rest.@idx.0
open unit 11 writ form name output/st/@{rand}/rest.@idx.1
dyna leap lang restart -
     tbath 300. rbuf 0. ilbfrq 0 iunrea 10 iunwri 11 -
     nstep @lnstep iuncrd -1 -
     npri 10000 time 0.001 iprfrq 20000 echeck 0. -
     lsta 0. lamb @lamb lsto 1.0  psta 0 pstop @lnstep pwind pssp

pert off

stop
