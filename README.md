Tutorial files for the 2019 San Sebastian DYNAPEUTICS summer school. These
supersede the version from 2016

Using the zeroth order test case of alchemical free energy simulations, the
calculation of the solvation free energy between ethane and methanol, the
setup of calculations using the PERT module of CHARMM (www.charmm.org) is
illustrated for both a single and a dual topology approach.

For further details see the various 00readme.txt files!