#!/bin/bash
#SBATCH -p lgpu                                                                #SBATCH --gres=gpu

# execute solv_st_step.inp
lneq=100000
lnstep=2000000
nlambda=21
###rand=${1:-1}
for rand in {1..8}
do
    mkdir -p "./solv/output/${rand}"
    mkdir -p "./solv/restart/${rand}"
done

start=0
stop=$((${nlambda}-1))
for ((i=${start};i<=${stop};i++))
do
    for rand in {1..8}
    do
	echo "idx:$i lneq:${lneq} lnstep:${lnstep} nlambda=${nlambda} rand:${rand}"
	OMP_NUM_THREADS=1 charmm mol:meh4 idx:$i lneq:${lneq} \
	    lnstep:${lnstep} nlambda=${nlambda} \
	    rand:${rand} -i solv_pert1.inp > solv/output/${rand}/out.$i.out&
    done
    wait
done

