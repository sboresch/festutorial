#!/bin/bash
#SBATCH -p lgpu
#SBATCH --gres=gpu
for i in 1 5 9
do
    OMP_NUM_THREADS=1 charmm mol:meh4 lnstep:2000000 rand:$i -i gasp_pert1.inp  > gasp/output/gasp_pert1.$i.out &
    OMP_NUM_THREADS=1 charmm mol:meh4 lnstep:2000000 rand:$(($i+1)) -i gasp_pert1.inp  > gasp/output/gasp_pert1.$(($i+1)).out &
    OMP_NUM_THREADS=1 charmm mol:meh4 lnstep:2000000 rand:$(($i+2)) -i gasp_pert1.inp  > gasp/output/gasp_pert1.$(($i+2)).out &
    OMP_NUM_THREADS=1 charmm mol:meh4 lnstep:2000000 rand:$(($i+3)) -i gasp_pert1.inp  > gasp/output/gasp_pert1.$(($i+3)).out 
    wait
done
