{
    dudl[++i]=$1;
}

END {
    ndat=i;
    delta=1/(ndat-1);

    sum1=dudl[1]/2.0;
    for (i=2;i<=ndat-1;i++) {
	sum1 += dudl[i];
    }
    sum1+=dudl[ndat]/2;
    da1=sum1*delta;

    sum2=dudl[1]/2.0;
    for (i=3;i<=ndat-2;i=i+2) {
	#print i;
	sum2 += dudl[i];
    }
    sum2+=dudl[ndat]/2;
    da2=sum2*delta*2;
    da=(4*da1-da2)/3;
    
#    print "DEBUG: ndat =", ndat;
#    printf("%9.3f %9.3f %9.3f\n", da1,da2,da);
    printf("da = %12.5f\n",da);
}
    
