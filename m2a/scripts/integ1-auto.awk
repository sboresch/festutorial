# as integ dat, but use raw PAVE values (unscale first)

{
    dudl[++i]=$1;
}

END {
    ndat=i;
    delta=1/(ndat-1);

    # unscale raw PAVEs
    dudl[1]=dudl[1]/(delta/2)
    dudl[ndat]=dudl[ndat]/(delta/2)
    for (i=2;i<=ndat-1;i++) {
	dudl[i]/=delta;
    }
    
    # now we continue as in integ1
    sum1=dudl[1]/2.0;
    for (i=2;i<=ndat-1;i++) {
	sum1 += dudl[i];
    }
    sum1+=dudl[ndat]/2;
    da1=sum1*delta;

    sum2=dudl[1]/2.0;
    for (i=3;i<=ndat-2;i=i+2) {
	#print i;
	sum2 += dudl[i];
    }
    sum2+=dudl[ndat]/2;
    da2=sum2*delta*2;
    da=(4*da1-da2)/3;
    
#    print "DEBUG: ndat =", ndat;
#    printf("%9.3f %9.3f %9.3f\n", da1,da2,da);
    printf("da = %9.3f\n",da);
}
    
