1) License 

The files in this directory, EXCEPT directory ./toppar and toppar.str
are

Copyright 2019, by Stefan Boresch

and are licensed under the Creative Commons Attribution 4.0
International (CC BY 4.0). To view a copy of the license, visit
https://creativecommons.org/licenses/by/4.0/

The files in toppar represent the CHARMM force field as of 2019 and
as distributed by jobs run on www.charmm-gui.org.

./toppar also contains params_naive_no_urey.dat and params_adapted_weak.dat
which contain parameters required for the dummy atom in the NH3D endstate.

2) What is this?

You find four major directories

    am
    met
    met2am_1
    met2am_2

'met' denotes methane, 'am' ammonia. In 'am' and 'met' you find scripts to
compute the absolute solvation free energies of ammonia and methane, respectively. The same small system as in the ethane to methanol 'minimal' example is
used, so the same warnings apply.

In 'met2am_1' and 'met2am_2' you find scripts to compute the relative
solvation free energy, using different treatments of the dummy atoms. Some
numbers shown in the main lecture were essentially generated this way.

The scripts 'gasp_pert1.inp' and 'solv_pert1.inp' are the main work
scripts in the gas phase and aqueous solution; they are quite similar to
gasp_st_ti.inp and solv_dt_ti_step.inp from the ethane to methanol examples.
The corresponding *sh scripts can be used to carry out the simulations.
NOTE: to run those in reasonable time, reduce the number of steps and
don't carry out multiple replicas. The scripts *.sh are finetuned to resources
on our cluster, adapt as needed.

In both relative free energy calculations all angles are kept

met2am_1 : The D-N-H equilibrium angle is that of H-C-H in methane; UB terms
           are removed.

met2am_2 : The D-N-H equilibrium angle is adapted so that the minimum energy
	   of NH3D equals that of real NH3. Further, the force constant is
	   significantly reduced; otherwise the relative solvation free
	   energy would still differ from the difference of the two absolute
	   solvation free energies.
