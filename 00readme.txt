Copyright 2016, 2019 by Stefan Boresch.  This work is licensed under the
Creative Commons Attribution 4.0 International (CC BY 4.0). To view a
copy of the license, visit https://creativecommons.org/licenses/by/4.0/.
For exceptions see correct/00license.txt

In this directory you find inputs for the 'alchemical free energy'
part of the 2019 DYNAPEUTICS summer school in San Sebastian.

The file

   ./fespractical_sanseb2016.pdf

contains the slides shown during the practical.

The model application is the "zeroth order" test case of alchemical FES, the
relative solvation free energy between ethane and methanol. In addition,
representative input for methane to ammonia (data for which were shown in
the main presentation) are provided

The free version of CHARMM is sufficient for all examples provided.

There are three directories:

   ./minimal (ethane to methanol)
   ./correct (ethane to methanol)
   ./m2a     (methane to ammonia)

During the practical, we'll only work in 'minimal'.  In minimal,
calculations are set up with a very small box and settings are chosen
to reduce runtime.

By contrast, 'correct' contains input for setting up the calculations
correctly, i.e., in line with accepted quality criteria (box size, PME
for electrostatics etc). Use these input files as templates
for any real work. You can run the simulations at your own
leisure and on your own resources. A parallel version of (free) CHARMM
is strongly recommended to speed things up!

If you have worked your way through 'minimial', you should also be able
to understand the inputs in 'm2a'. For further info, see m2a/00readme.txt!
